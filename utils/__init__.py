import urllib2
import subprocess

_MONGO = None


def get_mongo():
    from django.conf import settings
    from pymongo import MongoClient
    global _MONGO
    if _MONGO is None:
        if settings.MONGO_URL:
            # These are Heroku settings
            client = MongoClient(settings.MONGO_URL)
            _MONGO = client.get_default_database()
        else:
            client = MongoClient(
                host=settings.MONGODB_HOST, port=settings.MONGODB_PORT
            )
            _MONGO = client[settings.MONGODB_DATABASE_NAME]
    return _MONGO


def update_haystack_index():
    command = [
        'python', 'manage.py', 'update_index', 'songs'
    ]
    return subprocess.call(command)


def basic_authorization(user, password):
    s = user + ":" + password
    return "Basic " + s.encode("base64").rstrip()


def post(api_url, data, resource_name):
    req = urllib2.Request(
        api_url + resource_name, headers={"Content-Type": "application/json"},
        data=data
    )
    #req.get_method = lambda: 'PUT'

    f = urllib2.urlopen(req)
    return f


def get(api_url, resource_name=None):
    if not resource_name:
        resource_name = ''

    # If you do not pass the data argument, urllib2 uses a GET request
    req = urllib2.Request(
        api_url + resource_name, headers={"Content-Type": "application/json"}
    )
    # urllib2.urlopen("http://example.com/foo/bar").read()
    #req.get_method = lambda: 'PUT'

    f = urllib2.urlopen(req)
    return f.read()
