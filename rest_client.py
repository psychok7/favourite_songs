import json
from utils import post, get

API_URL = 'http://vagrant-khan:8000/api/v1/'


def create_song(api_url):
    data = json.dumps({
        'title': 'pira2', 'artist': 'piradinha2', 'album': 'killa'
    })
    p = post(api_url, data, resource_name='song/')
    print (p.info())


def retrieve_all_songs(api_url):
    songs = json.loads(get(api_url, resource_name='song/'))
    for song in songs['objects']:
        print ("Artist: ", song['artist'])
        print ("Title: ", song['title'])
        print ("Album: ", song['album'])
        print ("***********************")


def create_user(api_url):
    data = json.dumps({
        'username': 'mike2', 'email': 'mike2@gmail.com'
    })
    p = post(api_url, data, resource_name='user/')
    print (p.info())


def retrieve_all_users(api_url):
    users = json.loads(get(api_url, resource_name='user/'))
    for user in users['objects']:
        print ("Username: ", user['username'])
        print ("Email: ", user['email'])
        print ("***********************")


def create_favourite_song(api_url):
    data = json.dumps({
        'songs': '3', 'user_id': '4'
    })
    p = post(api_url, data, resource_name='favourite-songs/')
    print (p.info())


def retrieve_all_favourite_songs(api_url):
    favourite_songs = json.loads(get(api_url, resource_name='favourite-songs/'))
    for favourite_song in favourite_songs['objects']:
        print ("ID: ", favourite_song['_id'])
        print ("User ID: ", favourite_song['user_id'])
        print ("Songs: ", favourite_song['songs'])
        print ("***********************")


if __name__ == "__main__":
    # create_song(api_url=API_URL)
    # retrieve_all_songs(api_url=API_URL)
    # # create_user(api_url=API_URL)
    # retrieve_all_users(api_url=API_URL)
    # create_favourite_song(api_url=API_URL)
    retrieve_all_favourite_songs(api_url=API_URL)