from bson import ObjectId
import json
from tastypie.bundle import Bundle
from tastypie.resources import Resource
from tastypie import fields
from tastypie.authorization import Authorization
import urllib2
from utils import get_mongo
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from songs.models import Song


class MongoObject(object):

    def __init__(self, initial=None):
        self.__dict__['_data'] = {}

        if hasattr(initial, 'items'):
            self.__dict__['_data'] = initial

    def __getattr__(self, name):
        return self._data.get(name, None)

    def __setattr__(self, name, value):
        self.__dict__['_data'][name] = value

    def to_dict(self):
        return self._data


class FavouriteSongsResource(Resource):
    _id = fields.CharField(attribute='_id')
    user_id = fields.CharField(attribute='user_id')
    songs = fields.CharField(attribute='songs')

    class Meta:
        resource_name = 'favourite-songs'
        object_class = MongoObject
        authorization = Authorization()

    def _client(self):
        return get_mongo()

    def get_collection(self, collection_name):
        """
        Encapsulates collection name.
        """
        try:
            client = self._client()
            return client[collection_name]
        except AttributeError:
            raise ImproperlyConfigured("Define a collection in your resource.")

    # The following methods will need overriding regardless of your
    # data source.
    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj._id
        else:
            kwargs['pk'] = bundle_or_obj._id

        return kwargs

    def get_object_list(self, request):
        results = []
        db = self.get_collection(collection_name='user_favourite_songs')
        user_favourite_songs = db.find()
        for user_favourite_song in user_favourite_songs:
            new_obj = MongoObject(initial=user_favourite_song)
            results.append(new_obj)

        return results

    def obj_get_list(self, bundle, **kwargs):
        # Filtering disabled for brevity...
        return self.get_object_list(bundle.request)

    def obj_get(self, bundle, **kwargs):
        db = self.get_collection(collection_name='user_favourite_songs')
        user_favourite_songs = db.find_one({'_id': ObjectId(kwargs.get("pk"))})
        return MongoObject(initial=user_favourite_songs)

    def obj_create(self, bundle, **kwargs):
        bundle.obj = MongoObject(initial=kwargs)
        bundle = self.full_hydrate(bundle)
        db = self.get_collection(collection_name='user_favourite_songs')

        # songs is in fact songs_id, should be renamed
        song = get_object_or_404(Song, pk=kwargs.get('songs'))
        user = get_object_or_404(get_user_model(), pk=kwargs.get('user_id'))

        songs = []
        user_favourite_songs = db.find_one({'user_id': user.id})

        if user_favourite_songs:
            songs = user_favourite_songs['songs']

        songs.extend([song.id])

        db.update(
            {'user_id': user.id}, {'user_id': user.id, 'songs': songs},
            True
        )
        return bundle

    def obj_update(self, bundle, **kwargs):
        return self.obj_create(bundle, **kwargs)

    def obj_delete_list(self, bundle, **kwargs):
        pass

    def obj_delete(self, bundle, **kwargs):
        pass

    def rollback(self, bundles):
        pass


class AtracksResource(Resource):
    _id = fields.CharField(attribute='_id')
    track_id = fields.CharField(attribute='track_id')
    album_id = fields.CharField(attribute='album_id')

    class Meta:
        resource_name = 'atracks/external_api'
        object_class = MongoObject
        authorization = Authorization()

    def _client(self):
        return get_mongo()

    def get_collection(self, collection_name):
        """
        Encapsulates collection name.
        """
        try:
            client = self._client()
            return client[collection_name]
        except AttributeError:
            raise ImproperlyConfigured("Define a collection in your resource.")

    # The following methods will need overriding regardless of your
    # data source.
    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj._id
        else:
            kwargs['pk'] = bundle_or_obj._id

        return kwargs

    def get_object_list(self, request):
        results = []
        db = self.get_collection(collection_name='atracks')

        external_api_response = json.loads(
            urllib2.urlopen("http://freemusicarchive.org/recent.json").read()
        )
        for response in external_api_response['aTracks']:
            if response['track_id'] is not None and response['album_id'] is not None:
                db.update(
                    {
                        'track_id': response['track_id']},
                    {
                        'track_id': response['track_id'],
                        'album_id': response['album_id']},
                    True
                )

        atracks = db.find()
        for atrack in atracks:
            new_obj = MongoObject(initial=atrack)
            results.append(new_obj)

        return results

    def obj_get_list(self, bundle, **kwargs):
        # Filtering disabled for brevity...
        return self.get_object_list(bundle.request)

    def obj_get(self, bundle, **kwargs):
        db = self.get_collection(collection_name='atracks')
        user_favourite_songs = db.find_one({'_id': ObjectId(kwargs.get("pk"))})
        return MongoObject(initial=user_favourite_songs)

    def obj_create(self, bundle, **kwargs):
        bundle.obj = MongoObject(initial=kwargs)
        bundle = self.full_hydrate(bundle)
        db = self.get_collection(collection_name='atracks')

        db.update(
            {'track_id': bundle.obj.track_id},
            {'track_id': bundle.obj.track_id, 'album_id': bundle.obj.album_id},
            True
        )
        return bundle

    def obj_update(self, bundle, **kwargs):
        return self.obj_create(bundle, **kwargs)

    def obj_delete_list(self, bundle, **kwargs):
        pass

    def obj_delete(self, bundle, **kwargs):
        pass

    def rollback(self, bundles):
        pass