#!/usr/bin/env bash

sudo apt-get update
sudo apt-get -y install build-essential
sudo apt-get -y install git-core
sudo apt-get -y install python-pip python-dev
sudo apt-get -y install postgresql postgresql-contrib libpq-dev

# Install Elastic Search
cd ~
sudo apt-get install openjdk-7-jre-headless -y
wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.2.1.deb
sudo dpkg -i elasticsearch-1.2.1.deb
sudo service elasticsearch start

# Install MondoDB
sudo apt-get install mongodb -y
sudo service mongodb start