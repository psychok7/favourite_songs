from django.views.generic import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import get_user_model
from songs.models import Song
from utils import get_mongo


class UserCreateView(CreateView):
    template_name = 'users/user_create.html'
    model = get_user_model()
    fields = ['username', 'email']
    success_url = reverse_lazy('users-list')


class UserUpdateView(UpdateView):
    template_name = 'users/user_update.html'
    model = get_user_model()
    fields = ['username', 'email']
    success_url = reverse_lazy('users-list')


class UserDeleteView(DeleteView):
    template_name = 'users/user_delete.html'
    model = get_user_model()
    success_url = reverse_lazy('users-list')


class UserDetailView(DetailView):
    template_name = 'users/user_detail.html'
    model = get_user_model()

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        doc = self.get_object()
        db = get_mongo()["user_favourite_songs"]
        favourites = db.find_one({'user_id': doc.id})
        if favourites:
            songs = Song.objects.filter(id__in=favourites['songs'])
            context['favourite_songs'] = songs
        return context


class UserListView(ListView):
    template_name = 'users/user_list.html'
    model = get_user_model()