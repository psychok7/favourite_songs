from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from django.contrib.auth import get_user_model


class UserResource(ModelResource):
    class Meta:
        queryset = get_user_model().objects.all()
        resource_name = 'user'
        fields = ['username', 'email']
        authorization= Authorization()