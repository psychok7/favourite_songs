from django.conf.urls import patterns, url
from .views import (
    UserListView, UserCreateView, UserDetailView, UserUpdateView, UserDeleteView
)

urlpatterns = patterns(
    '',
    # # url(r'^$', SongListView.as_view(), name='songs-list'),
    url(r'^user/$', UserListView.as_view(), name='users-list'),
    url(r'^user/add/$', UserCreateView.as_view(), name='users-create'),
    url(
        r'^user/(?P<pk>[-_\w]+)/$', UserDetailView.as_view(),
        name='users-detail'
    ),
    url(
        r'^user/(?P<pk>[-_\w]+)/update/$', UserUpdateView.as_view(),
        name='users-update'
    ),
    url(
        r'^user/(?P<pk>[-_\w]+)/delete/$', UserDeleteView.as_view(),
        name='users-delete'
    ),
)
