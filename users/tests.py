from django.test import TestCase
from django.test.client import Client
from django.contrib.auth import get_user_model


class UserTestCase(TestCase):
    def setUp(self):
        get_user_model().objects.create(username="lionking", email="roar@example.com")

    def test_user_creation(self):
        c = Client()
        response = c.post(
            '/users/user/add/',
            {'username': 'john', 'email': 'smith@gmail.com'}, follow=True
        )
        self.assertEqual(response.status_code,  200)

    def test_user_update(self):
        c = Client()
        user = get_user_model().objects.get(username="lionking")
        response = c.post(
            '/users/user/' + str(user.id) + '/update/',
            {'username': 'john', 'email': 'smith@gmail.com'}, follow=True
        )
        self.assertEqual(response.status_code,  200)

    def test_user_delete(self):
        c = Client()
        user = get_user_model().objects.get(username="lionking")
        response = c.post(
            '/users/user/' + str(user.id) + '/delete/',
            {'username': 'john', 'email': 'smith@gmail.com'}, follow=True
        )
        self.assertEqual(response.status_code,  200)

    def test_user_api(self):
        c = Client()
        response = c.get('/api/v1/user/', {'format': 'json'})
        self.assertEqual(response.status_code,  200)