from django.db import models
from utils import update_haystack_index


class Song(models.Model):
    title = models.CharField(max_length=200)
    artist = models.CharField(max_length=200)
    album = models.CharField(max_length=200)
    song = models.FileField(null=True, blank=False, upload_to='songs/')
    updated_song = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __unicode__(self):
        return self.artist + ' - ' + self.title

    def save(self, *args, **kwargs):
        # Update Haystack every time a model is saved
        super(Song, self).save(*args, **kwargs)
        update_haystack_index()

    def delete(self, *args, **kwargs):
        # Update Haystack every time a model is deleted
        super(Song, self).delete(*args, **kwargs)
        update_haystack_index()
