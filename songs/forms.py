from django import forms
from haystack.forms import SearchForm


class SongSearchForm(SearchForm):

    def search(self):
        sqs = super(SongSearchForm, self).search()

        if not self.is_valid():
            return self.searchqueryset.all()

        if not self.cleaned_data['q']:
            return self.searchqueryset.all()

        return sqs
