from django.contrib import messages
from django.views.generic import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from .models import Song
from django.core.urlresolvers import reverse_lazy, reverse
from haystack.views import SearchView
from django.shortcuts import get_object_or_404
from django.views.generic.base import RedirectView
from django.contrib.auth import get_user_model
from utils import get_mongo


class SongCreateView(CreateView):
    template_name = 'songs/song_create.html'
    model = Song
    fields = ['title', 'artist', 'album']
    success_url = reverse_lazy('songs-list')


class SongUpdateView(UpdateView):
    template_name = 'songs/song_update.html'
    model = Song
    fields = ['title', 'artist', 'album']
    success_url = reverse_lazy('songs-list')


class SongDeleteView(DeleteView):
    template_name = 'songs/song_delete.html'
    model = Song
    success_url = reverse_lazy('songs-list')


class SongDetailView(DetailView):
    template_name = 'songs/song_detail.html'
    model = Song


class SongListView(ListView):
    template_name = 'songs/song_list.html'
    model = Song


class UserFavouriteSongRedirectView(RedirectView):
    permanent = False

    def get_redirect_url(self, **kwargs):
        return reverse("users-detail", args=(self.kwargs.get('user_id'), ))

    def get(self, request, *args, **kwargs):
        song = get_object_or_404(Song, pk=self.kwargs.get('song_id'))
        user = get_object_or_404(get_user_model(), pk=self.kwargs.get('user_id'))

        db = get_mongo()["user_favourite_songs"]
        songs = []
        user_favourite_songs = db.find_one({'user_id': user.id})

        if user_favourite_songs:
            songs = user_favourite_songs['songs']

        if song.id not in songs:
            # Don't allow duplicates
            songs.extend([song.id])

        db.update(
            {'user_id': user.id}, {'user_id': user.id, 'songs': songs},
            True
        )

        return super(UserFavouriteSongRedirectView, self).get(request, *args, **kwargs)


class HaystackSongsSearchView(SearchView):

    def extra_context(self):
        extra = super(HaystackSongsSearchView, self).extra_context()

        if self.request.GET.get('q'):
            extra['query'] = self.request.GET.get('q')

        if self.request.GET.get('user_wants_favourite'):
            extra['user_wants_favourite'] = self.request.GET.get(
                'user_wants_favourite'
            )

        return extra