from django.conf.urls import url
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
from haystack.query import SearchQuerySet
from tastypie.utils import trailing_slash
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from .models import Song
from utils import update_haystack_index


class SongResource(ModelResource):
    class Meta:
        queryset = Song.objects.all()
        resource_name = 'song'
        fields = ['title', 'artist', 'album']
        authorization = Authorization()

    def obj_create(self, bundle, **kwargs):
        bundle = super(SongResource, self).obj_create(bundle, **kwargs)
        # When object is created ensure its indexed
        update_haystack_index()
        return bundle

    def obj_delete(self, bundle, **kwargs):
        bundle = super(SongResource, self).obj_delete(bundle, **kwargs)
        # When object is deleted ensure its indexed
        update_haystack_index()
        return bundle

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/search%s$" % (
                    self._meta.resource_name, trailing_slash()
                ), self.wrap_view('get_search'), name="api_get_search"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query.
        sqs = SearchQuerySet().models(Song).load_all().auto_query(
            request.GET.get('q', '')
        )

        paginator = Paginator(sqs, 20)

        try:
            page = paginator.page(int(request.GET.get('page', 1)))
        except InvalidPage:
            raise Http404("Sorry, no results on that page.")

        objects = []

        for result in page.object_list:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)