from haystack import indexes
from .models import Song


class SongIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    id = indexes.CharField(model_attr='id')
    title = indexes.CharField(model_attr='title')
    artist = indexes.CharField(model_attr='artist')
    album = indexes.CharField(model_attr='album')

    def get_model(self):
        return Song

    def get_updated_field(self):
        return "updated_song"