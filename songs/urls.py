from django.conf.urls import patterns, url
from haystack.query import SearchQuerySet
from .models import Song
from .forms import SongSearchForm
from .views import (
    SongDetailView, SongListView, SongCreateView, SongDeleteView,
    SongUpdateView, HaystackSongsSearchView, UserFavouriteSongRedirectView
)

sqs = SearchQuerySet().models(Song)

urlpatterns = patterns(
    'haystack.views',
    url(
        r'^search/$',
        HaystackSongsSearchView(
            template='search/songs.html', searchqueryset=sqs,
            form_class=SongSearchForm
        ),
        name='haystack_songs_search'
    ),
    url(r'^song/$', SongListView.as_view(), name='songs-list'),
    url(r'^song/add/$', SongCreateView.as_view(), name='songs-create'),
    url(
        r'^song/(?P<pk>[-_\w]+)/$', SongDetailView.as_view(),
        name='songs-detail'
    ),
    url(
        r'^song/(?P<pk>[-_\w]+)/update/$', SongUpdateView.as_view(),
        name='songs-update'
    ),
    url(
        r'^song/(?P<pk>[-_\w]+)/delete/$', SongDeleteView.as_view(),
        name='songs-delete'
    ),
    url(
        r'song/(?P<song_id>\d+)/(?P<user_id>\d+)/accept/$',
        UserFavouriteSongRedirectView.as_view(),
        name='accepted_favourite_redirect'
    ),
)
