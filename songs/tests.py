from django.test import TestCase
from django.test.client import Client
from .models import Song
from django.contrib.auth import get_user_model
from utils import get_mongo


class SongTestCase(TestCase):
    def setUp(self):
        Song.objects.create(title="lion", artist="roar", album="meow")

    def test_users_add_favourite_songs(self):
        get_user_model().objects.create(username="lionking", email="roar@example.com")
        song = Song.objects.get(title="lion")
        user = get_user_model().objects.get(username="lionking")

        db = get_mongo()["user_favourite_songs_testing"]
        songs = []
        user_favourite_songs = db.find_one({'user_id': user.id})

        if user_favourite_songs:
            songs = user_favourite_songs['songs']

        if song.id not in songs:
            # Don't allow duplicates
            songs.extend([song.id])

        added = db.update(
            {'user_id': user.id}, {'user_id': user.id, 'songs': songs},
            True
        )
        self.assertEqual(added['ok'],  1.0)

    def test_song_creation(self):
        c = Client()
        response = c.post(
            '/songs/song/add/',
            {'title': 'john', 'artist': 'smith', 'album': 'luna'}, follow=True
        )
        self.assertEqual(response.status_code,  200)

    def test_song_update(self):
        c = Client()
        song = Song.objects.get(title="lion")
        response = c.post(
            '/songs/song/' + str(song.id) + '/update/',
            {'title': 'john', 'artist': 'smith', 'album': 'luna'}, follow=True
        )
        self.assertEqual(response.status_code,  200)

    def test_song_delete(self):
        c = Client()
        song = Song.objects.get(title="lion")
        response = c.post(
            '/songs/song/' + str(song.id) + '/delete/',
            {'title': 'john', 'artist': 'smith', 'album': 'luna'}, follow=True
        )
        self.assertEqual(response.status_code,  200)

    def test_song_haystack_search(self):
        c = Client()
        song = Song.objects.get(title="lion")
        response = c.get('/songs/search/', {'q': song.title})
        self.assertEqual(response.status_code,  200)

    def test_song_api(self):
        c = Client()
        response = c.get('/api/v1/song/', {'format': 'json'})
        self.assertEqual(response.status_code,  200)

    def test_song_api_search(self):
        c = Client()
        song = Song.objects.get(title="lion")
        response = c.get(
            '/api/v1/song/search/', {'format': 'json', 'q': song.title}
        )
        self.assertEqual(response.status_code,  200)

    def test_song_external_service(self):
        c = Client()
        response = c.get('/api/v1/atracks/external_api/', {'format': 'json'})
        self.assertEqual(response.status_code,  200)