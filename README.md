Django App to view users favourite songs


## Web App

- I am using SQL to save Songs and Users
- I am using MongoDB to save the favourite songs association with the user
- Im using Haystack for the search functionality


## REST API

Although i have a rest client you can query the API directly. Here are the links:

* External Service Integration, this fetches the atrack information from http://freemusicarchive.org/recent.json and saves to our MongoDB
http://favouritesongs.herokuapp.com/api/v1/atracks/external_api/?format=json

## LIST

- http://favouritesongs.herokuapp.com/api/v1/user/?format=json
- http://favouritesongs.herokuapp.com/api/v1/song/?format=json
- http://favouritesongs.herokuapp.com/api/v1/favourite-songs/?format=json

## SEARCH using haystack

- http://favouritesongs.herokuapp.com/api/v1/song/search/?format=json&q=maria


## CREATE

You can use the rest_client.py script


## REST CLIENT

The rest client has some examples on how to use the REST API with GET and POST