from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from songs.api import SongResource
from users.api import UserResource
from world.api import FavouriteSongsResource, AtracksResource
from world.views import HomepageView
from tastypie.api import Api

admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(SongResource())
v1_api.register(UserResource())
v1_api.register(FavouriteSongsResource())
v1_api.register(AtracksResource())

urlpatterns = patterns(
    '',
    url(r'^$', HomepageView.as_view(), name='homepage'),
    url(r'^songs/', include('songs.urls')),
    url(r'^users/', include('users.urls')),
    url(r'api/v1/doc/', include('tastypie_swagger.urls', namespace='tastypie_swagger')),
    (r'^api/', include(v1_api.urls)),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^admin/', include(admin.site.urls)),
)
